- Objplug - for the placement and export of physics, triggers, etc.
- Geomout - export level geometry
- Lmout - to export lightmaps

File Syntax:

_ff.txt - 'Underscore flatfile dot text'