fn FixName RealName OldPath NewPath = 
(
	--format "Try change from % to % at %\n" OldPath NewPath RealName
	FileName = filenameFromPath RealName
	PathName = getFilenamePath RealName
	
	fr = findString PathName OldPath
	if fr != undefined then
	(
		end = OldPath.count
		nn = replace PathName fr end NewPath
		nn += FileName
	)
	else
	(
		format "Can't find % in %\n" OldPath RealName
		nn = undefined
	)
	return nn
)

fn FixMaterial m OldPath NewPath =
(
	if m.diffuseMap != undefined  then
	(
		nn = FixName m.diffuseMap.filename OldPath NewPath
		if nn != undefined then
		(
			m.diffuseMap.filename = nn
		)
		else
		(
			--format "Can't fix diffuse texture % in %\n" m.diffuseMap.filename m.name
		)
	)
	else
	(
		--format "Can't fix diffuse texture % in %\n" m.diffuseMap.filename m.name
	)	
	if m.opacityMap != undefined  then
	(
		nn = FixName m.opacityMap.filename OldPath NewPath
		if nn != undefined then
		(
			m.opacityMap.filename = nn
		)
		else
		(
			--format "Can't fix opacity texture % in %\n" m.diffuseMap.filename m.name
		)
	)
	else
	(
		--format "Can't fix opacity texture % in %\n" m.diffuseMap.filename m.name
	)	
)

fn FixSubMaterial sm OldPath NewPath =
(
	for mm in sm.materialList do
	(
		if mm.diffuseMap != undefined  then
		(
			nn = FixName mm.diffuseMap.filename OldPath NewPath
			if nn != undefined then
			(
				mm.diffuseMap.filename = nn
			)
			else
			(
				--format "Can't fix diffuse texture % in % of %\n" mm.diffuseMap.filename mm.name sm.name
			)
		)
		else
		(
			--format "Can't fix diffuse texture % in % of %\n" mm.diffuseMap.filename mm.name sm.name
		)	
		if mm.opacityMap != undefined  then
		(
			nn = FixName mm.opacityMap.filename OldPath NewPath
			if nn != undefined then
			(
				mm.opacityMap.filename = nn
			)
			else
			(
				--format "Can't fix opacity texture % in % of %\n" mm.diffuseMap.filename mm.name sm.name
			)
		)
		else
		(
			--format "Can't fix opacity texture %s in % of %\n" mm.diffuseMap.filename mm.name sm.name
		)	
	)
)

fn ChangePath OldPath NewPath =
(
	for m in sceneMaterials do
	(
		b = m.ClassID[ 1 ] == multiSubMaterial.ClassID[ 1 ] and m.ClassID[ 2 ] == multiSubMaterial.ClassID[ 2 ]
		if b do FixSubMaterial m OldPath NewPath
		
		b = m.ClassID[ 1 ] == StandardMaterial.ClassID[ 1 ] and m.ClassID[ 2 ] == StandardMaterial.ClassID[ 2 ]
		if b do FixMaterial m OldPath NewPath
	)
)

rollout unnamedRollout "Untitled" width:162 height:281
(
	edittext edt1 "" pos:[8,56] width:143 height:18
	label lbl1 "Old path" pos:[9,36] width:75 height:18
	edittext edt2 "" pos:[10,112] width:143 height:18
	label lbl2 "New path" pos:[11,92] width:75 height:18
	button btn1 "Update" pos:[32,168] width:96 height:26
	button btn3 "Cancel" pos:[35,215] width:96 height:26
	on btn1 pressed do
	(
		ChangePath edt1.text edt2.text
	)
	on btn3 pressed do
	(
		DestroyDialog unnamedRollout
	)
)

CreateDialog unnamedRollout width:162 height:300
