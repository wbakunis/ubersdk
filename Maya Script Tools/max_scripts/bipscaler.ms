--
-- Bip scaler
-- Coded by AND 2004
--

fScaleFactor = 2
bipNode = $Bip01_lo
global fOriginalHeight

-- list of normal bones which parents are Biped
refList = #()
posList = #()
sclList = #()

--
-- Create ref list
--
function MakeRefBones node =
(
	if classof node == BoneGeometry then
	(
		Append refList node
		local point3 vtx
		vtx = node.transform.pos - node.parent.transform.pos
		Append posList vtx
		Append sclList node.transform.scale
		return 0
	)
	
	for i = 1 to node.children.count do
	(
		MakeRefBones( node.children[ i ] )
	)
)

--
-- Scale all attached bones
--
function ScaleRefBones node =
(
	node.controller.height = fOriginalHeight * fScaleFactor

	for i = 1 to refList.count do
	(
		refList[ i ].scale = sclList[ i ] * fScaleFactor
		refList[ i ].pos = refList[ i ].parent.transform.pos + posList[ i ] * fScaleFactor
	)
)

--
-- Get root node
--
function GetRootNode node =
(
	ptr = node
	while ptr.parent != undefined do
	(
		ptr = ptr.parent
	)
	
	return ptr
)

--
-- Dialog window
--
rollout bip2odedlg "BIPED scaler v1.01 by AND" width:233 height:120
(
	local sp1
	pickbutton btn_skeletalsys "Select Biped" pos:[26,22] width:176 height:31
	spinner btn_scale "Scale" fieldwidth:60 pos:[26,77] type:#float range:[0.001,10000,1] enabled:false
	button btn_cancel "Exit" pos:[132,69] width:70 height:31
	on btn_skeletalsys picked obj do
	(
		sp1 = GetRootNode obj
		if classof sp1 == Biped_Object then
		(
			btn_skeletalsys.text = sp1.name + " (" + obj.name + ")"
			btn_scale.enabled = true
			
			bipNode = sp1
			refList = #()
			posList = #()
			sclList = #()
			sp1.controller.figuremode = true
			MakeRefBones sp1
			fOriginalHeight = sp1.controller.height
		)
	)
	on btn_scale changed val do
	(
		fScaleFactor = val
		ScaleRefBones bipNode
	)
	on btn_cancel pressed do
		DestroyDialog bip2odedlg
)

--
-- Main
--
CreateDialog bip2odedlg width:233 height:120
