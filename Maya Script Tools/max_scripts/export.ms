ofile

fn SaveMaterial m =
(
	format "standard\n" to:ofile
	if m.diffuseMap != undefined  then
	(
		format "%\n" m.diffuseMap.filename to:ofile
	)
	else
	(
		format "none\n" to:ofile
	)	
	if m.opacityMap != undefined  then
	(
		format "%\n" m.opacityMap.filename to:ofile
	)
	else
	(
		format "none\n" to:ofile
	)	
)

fn SaveSumMaterial sm =
(
	format "multi\n%\n" sm.numsubs to:ofile
	
	for mm in sm.materialList do
	(
		format "%\n" mm.name to:ofile 
		if mm.diffuseMap != undefined  then
		(
			format "%\n" mm.diffuseMap.filename to:ofile
		)
		else
		(
			format "none\n" to:ofile
		)	
		if mm.opacityMap != undefined  then
		(
			format "%\n" mm.opacityMap.filename to:ofile
		)
		else
		(
			format "none\n" to:ofile
		)	
	)
)

fn SaveMaterials =
(
	--format "%\n" meditMaterials.count to:ofile
	--for m in meditMaterials do
	format "%\n" sceneMaterials.count to:ofile
	for m in sceneMaterials do
	(
		format "%\n" m.name to:ofile 
		b = m.ClassID[ 1 ] == multiSubMaterial.ClassID[ 1 ] and m.ClassID[ 2 ] == multiSubMaterial.ClassID[ 2 ]
		if b do SaveSumMaterial( m )
		
		b = m.ClassID[ 1 ] == StandardMaterial.ClassID[ 1 ] and m.ClassID[ 2 ] == StandardMaterial.ClassID[ 2 ]
		if b do SaveMaterial( m )
	)
)

fn ExportLight node =
(
	format "light\n" to:ofile
	format "%\n" node.name to:ofile
	
	if node.type == #omni then
	(
		print	omni to:ofile
	)else
	
	if node.type == #freeSpot then
	(
		print	freeSpot to:ofile
	)else
	
	if node.type == #targetSpot then
	(
		print	targetSpot to:ofile
	)else
	
	if node.type == #freeDirect then
	(
		print	freeDirect to:ofile
	)else
	
	if node.type == #targetDirect then
	(
		print	targetDirect to:ofile
	)
	
	matr = node.objecttransform
	format "% % %\n" matr.row1.x matr.row1.y matr.row1.z to:ofile
	format "% % %\n" matr.row2.x matr.row2.y matr.row2.z to:ofile
	format "% % %\n" matr.row3.x matr.row3.y matr.row3.z to:ofile
	format "% % %\n" matr.row4.x matr.row4.y matr.row4.z to:ofile
)

ExportNode

fn ExportHelper node =
(
	if node.children.count > 0 do
	(
		format "room\n" to:ofile
		format "%\n" node.name to:ofile
		print node.children.count to:ofile
		for i = 1 to node.children.count do ExportNode node.children[ i ]
	)
)

fn ExportMesh node =
(
	if node.IsTarget == false then
	(
		local mesh = node.mesh
		format "mesh\n" to:ofile
		format "%\n" node.name to:ofile
		format "%\n" node.material.name to:ofile
		nv = mesh.numverts
		nf = mesh.numfaces
		ntv = getNumTVerts mesh
		format "% % %\n" nv nf ntv to:ofile
		local	i;
		local	verts = #()
		local	n_fac
		verts.count = nv
		for i = 1 to nv do
		(
			v = at time 0 (in coordsys local getVert mesh i) * node.objecttransform
			verts[ i ] = v
			format "% % %\n" v.x v.y v.z to:ofile
		)
		for i = 1 to nf do
		(
			smg = getFaceSmoothGroup mesh i
			fnm = at time 0 (in coordsys local meshop.getFaceRnormals mesh i)
			local trans = at time 0 node.objecttransform
			trans.translation = [0, 0, 0]
			local	n1, n2, n3
			n_fac = at time 0 (in coordsys local getFaceNormal mesh i) * trans
			if fnm.count > 0 then
			(
				n1 = fnm[1] * trans
				n2 = fnm[2] * trans
				n3 = fnm[3] * trans
			)
			else
			(
				n1 = n_fac
				n2 = n_fac
				n3 = n_fac
			)
			f = getFace mesh i
			mat = getFaceMatID mesh i
			rfn = cross ( verts[f.y] - verts[f.x] ) ( verts[f.z] - verts[f.x] )
			d = dot rfn n_fac
			if d > 0 then
			(
				format "% % % % %\n" f.x f.y f.z smg mat to:ofile
				format "% % %\n" n1.x n1.y n1.z to:ofile
				format "% % %\n" n2.x n2.y n2.z to:ofile
				format "% % %\n" n3.x n3.y n3.z to:ofile
			)
			else
			(
				format "% % % % %\n" f.x f.z f.y smg mat to:ofile
				format "% % %\n" n1.x n1.y n1.z to:ofile
				format "% % %\n" n3.x n3.y n3.z to:ofile
				format "% % %\n" n2.x n2.y n2.z to:ofile
			)
		)
		for i = 1 to ntv do
		(
			v = getTVert mesh i
			format "% %\n" v.x v.y to:ofile
		)
		for i = 1 to nf do
		(
			local trans = at time 0 node.objecttransform
			trans.translation = [0, 0, 0]
			n_fac = at time 0 (in coordsys local getFaceNormal mesh i) * trans
			
			f = getFace mesh i
			rfn = cross ( verts[f.y] - verts[f.x] ) ( verts[f.z] - verts[f.x] )
			d = dot rfn n_fac
			f = getTVFace mesh i
			if d > 0 then
			(
				format "% % %\n" f.x f.y f.z to:ofile
			)
			else
			(
				format "% % %\n" f.x f.z f.y to:ofile
			)
		)
	)
	else
	(
		format "target\n" to:ofile
		print node.name to:ofile
		format "% % %\n" node.pos.x node.pos.y node.pos.z to:ofile
	)
)

fn ExportNode node =
(
	case superclassof node of
	(
		'helper': ExportHelper node
		'light': ExportLight node
		'GeometryClass': ExportMesh node
	)
)

fn Export =
(
	for i = 1 to rootNode.children.count do ExportNode rootNode.children[ i ]
)

tfname = getSaveFileName caption:"Export scene" filename:"output.scn" types:"*.scn"
if tfname != undefined do
(
	ofile = createfile tfname
	
	if ofile != undefined do
	(
		SaveMaterials()
		Export()
	)
	
	close ofile
)
