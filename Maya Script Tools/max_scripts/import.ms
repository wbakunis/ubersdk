ofile
i = 0

fn LoadMaterials =
(
	local	nmat = ( (readLine ofile) as Integer)
	for i = 1 to nmat do
	(
		name = readLine ofile
		type = readLine ofile
		if type == "multi" then
		(
			ns = readValue ofile
			sm = multimaterial numsubs:ns name:name
			for j = 1 to ns do
			(
				mname = readLine ofile
				mapd = readLine ofile
				mapo = readLine ofile
				if mapd != "none" then
				(
					d = bitmaptex filename:mapd diffuseMapEnable:true
				)
				else
				(
					d = undefined
				)
				if mapo != "none" then
				(
					o = bitmaptex filename:mapo
				)
				else
				(
					o = undefined
				)
				sm[ j ] = standard diffuseMap:d opacityMap:o
				sm[ j ].name = mname
				sm[ j ].showInViewport = true
				
				if d != undefined do showTextureMap sm[ j ] d on
				if o != undefined do showTextureMap sm[ j ] o on
			)
			--sm.showInViewport = true
			--setMeditMaterial i sm
			append sceneMaterials sm
		)	
		else
		(
			mapd = readLine ofile
			mapo = readLine ofile
			if map != "none" then
			(
				d = bitmaptex filename:mapd diffuseMapEnable:true
			)
			else
			(
				d = undefined
			)
			if mapo != "none" then
			(
				o = bitmaptex filename:mapo
			)
			else
			(
				o = undefined
			)
			m = standard diffuseMap:d opacityMap:o
			m.name = name
			m.showInViewport = true
			--setMeditMaterial i m
			append sceneMaterials m
		)
	)
)

numobjects = 1
lobjects = #( rootNode )

fn LoadObjects =
(
	local type = readLine ofile
	local name = readLine ofile
	
	if type == "room" then
	(
		local	chld
		local	numchilds = readValue ofile
		
		numobjects = 1
		lobjects.count = numchilds
		
		format "room % %\n" name numchilds
		
		for chld = 1 to numchilds do
		(
			LoadObjects()
			numobjects += 1
		)
		clearSelection()
		for chld = 1 to numchilds do
		(
			if lobjects[ chld ] != undefined do
				selectMore lobjects[ chld ]
		)
		group selection name:name
		clearSelection()
	)
	else
	(
		--local ltype = readLine ofile
		if type == "mesh" then
		(
			local	i, mi, smi, found, j
			local	matname = readLine ofile
			local	nv = readValue ofile
			local	nf = readValue ofile
			local	ntv = readValue ofile
			local	m
			verts = #()
			faces = #()
			sgs = #()
			mtls = #()
			nmls = #()
			tvrts = #()
			mi = -1
			smi = -1
			found = false
			for i = 1 to sceneMaterials.count do
			(
				m = sceneMaterials[ i ]
				b = m.ClassID[ 1 ] == multiSubMaterial.ClassID[ 1 ] and m.ClassID[ 2 ] == multiSubMaterial.ClassID[ 2 ]
				
				if b do
				(
					if m.name == matname then
					(
						mi = i
						found = true
					)
					else
					(
						for j = 1 to m.numsubs do
						(
							if matname == m.names[ j ] do
							(
								mi = i
								smi = j
								found = true
								break
							)
						)
					)
				)
		
				b = m.ClassID[ 1 ] == StandardMaterial.ClassID[ 1 ] and m.ClassID[ 2 ] == StandardMaterial.ClassID[ 2 ]
		
				if b do
					if matname == m.name do
					(
						mi = i;
						found = true
						break;
					)
					
				if found do break
			)
			
			if found == false do format "Not found for %\n" matname
			
			verts.count = nv
			faces.count = nf
			sgs.count = nf
			mtls.count = nf
			nmls.count = 3 * nf
			tvrts.count = ntv
			for i = 1 to nv do
			(
				x = readValue ofile
				y = readValue ofile
				z = readValue ofile
				verts[ i ] = [ x, y, z ]
			)
			for i = 1 to nf do
			(
				v1 = readValue ofile
				v2 = readValue ofile
				v3 = readValue ofile
				sg = readValue ofile
				mt = readValue ofile
				
				n = [0,0,0]
				
				n.x = readValue ofile
				n.y = readValue ofile
				n.z = readValue ofile
				nmls[ i * 3 - 2 ] = n
				n.x = readValue ofile
				n.y = readValue ofile
				n.z = readValue ofile
				nmls[ i * 3 - 1 ] = n
				n.x = readValue ofile
				n.y = readValue ofile
				n.z = readValue ofile
				nmls[ i * 3 - 0 ] = n
				
				faces[ i ] = [ v1, v2, v3 ]
				sgs[ i ] = sg
				mtls[ i ] = mt
			)
			for i = 1 to ntv do
			(
				n = [0,0,0]
				n.x = readValue ofile
				n.y = readValue ofile
				n.z = 0
				tvrts[ i ] = n
			)
			m = undefined
			if mi >= 0 then
			(
				if smi >= 0 then
				(
					m = sceneMaterials[mi].materialList[smi]
				)
				else
				(
					m = sceneMaterials[mi]
				)
			)
			
			new_mesh = mesh vertices:verts faces:faces name:name materialIDs:mtls tverts:tvrts material:m
			
			buildTVFaces new_mesh false
			for i = 1 to nf do
			(
				setFaceSmoothGroup new_mesh i sgs[ i ]
				setNormal new_mesh faces[ i ].x nmls[ i * 3 - 2 ]
				setNormal new_mesh faces[ i ].y nmls[ i * 3 - 1 ]
				setNormal new_mesh faces[ i ].z nmls[ i * 3 - 0 ]
				n = [0,0,0]
				n.x = readValue ofile
				n.y = readValue ofile
				n.z = readValue ofile
				setTVFace new_mesh i n
			)
			update new_mesh
			lobjects[ numobjects ] = new_mesh
		)else
		
		if type == "light" then
		(
			local	ltype = readLine ofile
			local	n = [0,0,0]
			n.x = readValue ofile
			n.y = readValue ofile
			n.z = readValue ofile
			n.x = readValue ofile
			n.y = readValue ofile
			n.z = readValue ofile
			n.x = readValue ofile
			n.y = readValue ofile
			n.z = readValue ofile
			n.x = readValue ofile
			n.y = readValue ofile
			n.z = readValue ofile
		)else
		
		if type == "target" then
		(
			local	lpos = [ 0, 0, 0 ]
			--local	name = readLine ofile
			lpos.x = readValue ofile
			lpos.y = readValue ofile
			lpos.z = readValue ofile
			t = targetobject pos: lpos
			new_target =  targetobject pos: lpos name:name
			lobjects[ numobjects ] = new_target
		)
		
		
	)
)

tfname = getOpenFileName caption:"Export scene" filename:"output.scn" types:"*.scn"
if tfname != undefined do
(
	ofile = openfile tfname
	
	if ofile != undefined then
	(
		LoadMaterials()
		LoadObjects()
		close ofile
	)
)
