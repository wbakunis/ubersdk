--
-- Biped to ODE exporter v0.02
-- by AND 2003
--

macroScript Scene2ODE
	category:"MAX Script Tools"
	internalcategory:"MAX Script Tools"
	buttontext:"Scene to ode"
	toolTip:"Scene to ode" 
(



file
g_nNodeNumber
g_nPassNumber

global bip2odedlg

--
-- Fix node name up
--
function FixupNodename name =
(
	str = name
	
	for i = 1 to str.count do
	(
		if str[ i ] == " " then
			str[ i ] = "_"
	)
	
	return str
)

--
-- Calculate node local bbox
--
vmin
vmax
vv

function CalculateLocalBBox node =
(	
	mat = inverse node.transform
	
	vmin = Point3 1e30 1e30 1e30
	vmax = Point3 -1e30 -1e30 -1e30
	
	for i = 1 to node.mesh.numVerts do
	(
		v = (in coordsys local getVert node.mesh i) * node.objecttransform
		v = v * mat

		if v.x < vmin.x then vmin.x = v.x
		if v.y < vmin.y then vmin.y = v.y
		if v.z < vmin.z then vmin.z = v.z
		if v.x > vmax.x then vmax.x = v.x
		if v.y > vmax.y then vmax.y = v.y
		if v.z > vmax.z then vmax.z = v.z
	)
	
	vmin.x = vmin.x * node.scale.x
	vmin.y = vmin.y * node.scale.y
	vmin.z = vmin.z * node.scale.z
	
	vmax.x = vmax.x * node.scale.x
	vmax.y = vmax.y * node.scale.y
	vmax.z = vmax.z * node.scale.z
)
			
--
-- Export initial set of bones
--
function EvaluateNode node =
(
	if superclassof node == GeometryClass then
	(
		if g_nPassNumber == 0 then
		(
			g_nNodeNumber = g_nNodeNumber + 1
		)
		else
		(
			currName = FixupNodename node.name
			
			format "Bone % % \"%\"\n" g_nNodeNumber -1 currName to:file

			qq = node.transform.rotationpart
			mat3 = qq as matrix3

			
			format "mrot % % %\n" mat3.row1.x mat3.row1.z mat3.row1.y to:file
			format "     % % %\n" mat3.row3.x mat3.row3.z mat3.row3.y to:file
			format "     % % %\n" mat3.row2.x mat3.row2.z mat3.row2.y to:file

			format "pos  % % %\n" node.transform.row4.x node.transform.row4.z node.transform.row4.y to:file
			
			CalculateLocalBBox( node )
			format "min  % % %\n" vmin.x vmin.z vmin.y to:file
			format "max  % % %\n" vmax.x vmax.z vmax.y to:file
			
			format "\n" to:file
	
			g_nNodeNumber = g_nNodeNumber + 1
		)
	)
	
	for i = 1 to node.children.count do
	(
		EvaluateNode( node.children[ i ] )
	)
)

--
-- Main
--
function DoIt =
(
	tmpfilename = getSaveFileName caption:"SCENE to ODE exporter v0.02" filename:"scene.bpo" types:"*.bpo"
	if tmpfilename != undefined then
	(
		filename = getFilenamePath tmpfilename + getFilenameFile tmpfilename + ".bpo"
		
		file = createfile filename
		--bipObj = $bip01
		
		-- start the process
		format "//\n// SCENE to ODE exporter v0.02\n//\n\n" to:file
		
		g_nNodeNumber = 0
		g_nPassNumber = 0
		EvaluateNode( rootNode )

		format "numBones %\n\n" g_nNodeNumber to:file
		
		g_nNodeNumber = 0
		g_nPassNumber = 1
		EvaluateNode( rootNode )
		
		close file
	)
	
--	DestroyDialog bip2odedlg
)

--
-- Get root node
--
function GetRootNode node =
(
	ptr = node
	while ptr.parent != undefined do
	(
		ptr = ptr.parent
	)
	
	return ptr
)

DoIt()


)
