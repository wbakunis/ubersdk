--
-- Biped to ODE exporter
-- by AND 2003,2004
--

-- global defined variables
g_pScriptName = "BIPED to ODE exporter v0.04"
file
g_nNodeNumber
g_nPassNumber
global bip2odedlg

-- bone structure
struct TBone ( name, index = -1, parent = -1 )

-- list of all bones required for ODE
odeSet = #()

--
-- Find bone in TBone array
--
function FindInTBones co str =
(
	for i = 1 to co.count do
	(
		if str == co[ i ].name then
		(
			return i
		)
	)
	
	return 0
)

--
-- Fix node name up
--
function FixupNodename name =
(
	if name[ 1 ] == "#" then
	(
		str = SubString name 2 (name.count - 1)
	)
	else
	(
		str = name
	)
	
	for i = 1 to str.count do
	(
		if str[ i ] == " " then
			str[ i ] = "_"
	)
	
	return str
)

--
-- Calculate node local bbox
--
vmin
vmax

function CalculateLocalBBox node =
(	
	mat = inverse node.transform
	
	vmin = Point3 1e30 1e30 1e30
	vmax = Point3 -1e30 -1e30 -1e30
	
	for i = 1 to node.mesh.numVerts do
	(
		v = (in coordsys local getVert node.mesh i) * node.objecttransform
		v = v * mat

		if v.x < vmin.x then vmin.x = v.x
		if v.y < vmin.y then vmin.y = v.y
		if v.z < vmin.z then vmin.z = v.z
		if v.x > vmax.x then vmax.x = v.x
		if v.y > vmax.y then vmax.y = v.y
		if v.z > vmax.z then vmax.z = v.z
	)
)

--
-- Make node list
--
function MakeNodeList node =
(
	if node.name[ 1 ] == "#" then
	(
		bn = TBone node.name
		Append odeSet bn
	)

	for i = 1 to node.children.count do
	(
		MakeNodeList( node.children[ i ] )
	)
)

--
-- Check for active parent
--
function CheckForParent node =
(
	curr = node.parent
	while curr != undefined do
	(
		pidx = FindInTBones odeSet curr.name
		if pidx > 0 then
		(
			return pidx
		)
		
		curr = curr.parent
	)
	
	return 0
)

--
-- Export initial set of bones
--
function EvaluateNode node =
(
	index = FindInTBones odeSet node.name

	if index > 0 then
	(
		if g_nPassNumber == 0 then
		(
			odeSet[ index ].index = g_nNodeNumber
	
			if node.parent != undefined then
			(
				pidx = CheckForParent node
				if pidx > 0 then
				(
					odeSet[ index ].parent = odeSet[ pidx ].index
				)
			)
		)
		else
		(
			currName = FixupNodename node.name
			
			format "Bone % % \"%\"\n" odeSet[ index ].index odeSet[ index ].parent currName to:file
			format "mrot % % %\n" node.transform.row1.x node.transform.row1.z node.transform.row1.y to:file
			format "     % % %\n" node.transform.row3.x node.transform.row3.z node.transform.row3.y to:file
			format "     % % %\n" node.transform.row2.x node.transform.row2.z node.transform.row2.y to:file
			format "pos  % % %\n" node.transform.row4.x node.transform.row4.z node.transform.row4.y to:file
			
			CalculateLocalBBox( node )
			format "min  % % %\n" vmin.x vmin.z vmin.y to:file
			format "max  % % %\n" vmax.x vmax.z vmax.y to:file
			
			format "\n" to:file
		)

		g_nNodeNumber = g_nNodeNumber + 1
	)
	
	for i = 1 to node.children.count do
	(
		EvaluateNode( node.children[ i ] )
	)
)

--
-- Main
--
function DoIt bipObj =
(
	tmpfilename = getSaveFileName caption:g_pScriptName filename:"biped.bpo" types:"*.bpo"
	if tmpfilename != undefined then
	(
		filename = getFilenamePath tmpfilename + getFilenameFile tmpfilename + ".bpo"
		
		file = createfile filename
		
		-- start the process
		format "//\n" to:file
		format "// %\n" g_pScriptName to:file
		mname = maxFilePath + maxFileName
		format "// Exported from : \"%\"\n" mname to:file
		format "//\n\n" to:file
		
		-- claculate set of objects to be exported
		odeSet = #()
		MakeNodeList( bipObj )
		
		g_nNodeNumber = 0
		g_nPassNumber = 0
		EvaluateNode( bipObj )

		format "numBones %\n\n" g_nNodeNumber to:file
		
		g_nNodeNumber = 0
		g_nPassNumber = 1
		EvaluateNode( bipObj )
		
		close file
	)
	
	DestroyDialog bip2odedlg
)

--
-- Get root node
--
function GetRootNode node =
(
	ptr = node
	while ptr.parent != undefined do
	(
		ptr = ptr.parent
	)
	
	return ptr
)

--
-- Dialog window
--
rollout bip2odedlg g_pScriptName width:233 height:120
(
	local sp1
	pickbutton btn_skeletalsys "Select skeletal system" pos:[26,22] width:176 height:31
	button btn_export "Export" pos:[26,69] width:80 height:31 enabled:false
	button btn_cancel "Cancel" pos:[122,69] width:80 height:31
	on btn_skeletalsys picked obj do
	(
		sp1 = GetRootNode obj
		btn_skeletalsys.text = sp1.name + " (" + obj.name + ")"
		btn_export.enabled = true
	)
	on btn_export pressed do
		DoIt sp1
	on btn_cancel pressed do
		DestroyDialog bip2odedlg
)

CreateDialog bip2odedlg width:233 height:120
