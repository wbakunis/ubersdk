--
-- Biped to ODE exporter v0.02
-- by AND 2003
-- by Magum 2004
--


macroScript System2ODE
	category:"MAX Script Tools"
	internalcategory:"MAX Script Tools"
	buttontext:"System to ode"
	toolTip:"System to ode" 
(




file
g_nNodeNumber
g_nJointNumber
g_nPassNumber

global bip2odedlg

--
-- Fix node name up
--
function FixupNodename name =
(
	str = name
	
	for i = 1 to str.count do
	(
		if str[ i ] == " " then
			str[ i ] = "_"
	)
	
	return str
)

--
-- Export initial set of bones
--
function EvaluateNode node =
(
	if classof node == XSimplePhysic then
	(
		if g_nPassNumber == 0 then
		(
			g_nNodeNumber = g_nNodeNumber + 1
		)
		else
		(
			currName = FixupNodename node.name
			
			format "Bone % % \"%\"\n" g_nNodeNumber -1 currName to:file

			qq = node.transform.rotationpart
			mat3 = qq as matrix3

			
			format "mrot % % %\n" mat3.row1.x mat3.row1.z mat3.row1.y to:file
			format "     % % %\n" mat3.row3.x mat3.row3.z mat3.row3.y to:file
			format "     % % %\n" mat3.row2.x mat3.row2.z mat3.row2.y to:file

			format "pos  % % %\n" node.transform.row4.x node.transform.row4.z node.transform.row4.y to:file
			
			format "mif %\n" node.fileMif to:file
			format "file %\n" node.nameCph to:file
			
			format "\n\n" to:file
	
			g_nNodeNumber = g_nNodeNumber + 1
		)
	)
	
	for i = 1 to node.children.count do
	(
		EvaluateNode( node.children[ i ] )
	)
)


--
-- Export initial set of bones
--
function EvaluateJoints node =
(
	if classof node == XPhysicJoint then
	(
		if g_nPassNumber == 0 then
		(
			g_nJointNumber = g_nJointNumber + 1
		)
		else
		(
			currName = FixupNodename node.name
			
			format "Joint % \"%\"\n" g_nJointNumber currName to:file
			
			format "anchor  % % %\n" node.transform.row4.x node.transform.row4.z node.transform.row4.y to:file
			
			format "type %\n" node.jointType to:file
			
			if node.useCfm then
			(
				format "cfmIsSet 1 %\n" node.cfm to:file
				
			) else
			(
				format "cfmIsSet 0\n" to:file
			)
			
			if node.useErp then
			(
				format "erpIsSet 1 %\n" node.erp to:file
				
			) else
			(
				format "erpIsSet 0\n" to:file
			)
			

			format "axis1 % % %\n" 0.0 0.0 0.0 to:file
			format "axis2 % % %\n" 0.0 0.0 0.0 to:file
			
--			format "stops1 % % %\n" 0 0.0 0.0 to:file
--			format "stops2 % % %\n" 0 0.0 0.0 to:file
--			format "stops3 % % %\n" 0 0.0 0.0 to:file
			format "stops1 %\n" 0 to:file
			format "stops2 %\n" 0 to:file
			format "stops3 %\n" 0 to:file



			currName = FixupNodename node.nodeFirst.name
			format "body1 \"%\"\n" currName  to:file
			
			currName = FixupNodename node.nodeSecond.name
			format "body2 \"%\"\n" currName to:file

			format "\n\n" to:file
	
			g_nJointNumber = g_nJointNumber + 1
		)
	)
	
	for i = 1 to node.children.count do
	(
		EvaluateJoints( node.children[ i ] )
	)
)


--
-- Main
--
function DoIt =
(
	tmpfilename = getSaveFileName caption:"SCENE to ODE exporter v0.05" filename:"scene.sph" types:"*.sph"
	if tmpfilename != undefined then
	(
		filename = getFilenamePath tmpfilename + getFilenameFile tmpfilename + ".sph"
		
		file = createfile filename
		--bipObj = $bip01
		
		-- start the process
		format "//\n// Physic scene exporter v0.05\n//\n\n" to:file
		
		g_nNodeNumber = 0
		g_nPassNumber = 0
		EvaluateNode( rootNode )

		format "numBones %\n\n" g_nNodeNumber to:file
		
		g_nNodeNumber = 0
		g_nPassNumber = 1
		EvaluateNode( rootNode )
		
		g_nPassNumber = 0
		g_nJointNumber = 0
		EvaluateJoints( rootNode )
		
		format "\n\n" to:file
		
		format "numJoints %\n\n" g_nJointNumber to:file

		
		g_nPassNumber = 1
		g_nJointNumber = 0
		EvaluateJoints( rootNode )

		
		close file
	)
	
--	DestroyDialog bip2odedlg
)

--
-- Get root node
--
function GetRootNode node =
(
	ptr = node
	while ptr.parent != undefined do
	(
		ptr = ptr.parent
	)
	
	return ptr
)

DoIt()

)