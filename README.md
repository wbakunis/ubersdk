# README #
Files being hosted on BitBucket for preservation. UberSoldier 2 SDK was found on the defunct website using WayBack Machine. 


### Developer Config Directory:
Copy and paste these two files in UberSoldier's install folder. Start the game with ** *"ef_full_win.bat"* **. You could also just copy the ini
to savegame location ** *('user\Documents\US2')* **. Make sure to create a backup of this config because you will need to rename ** *"config.fill.windows.ini"* ** to ** *"config.ini"* **

### Developer Mode:
> 1. Make sure you're running UberSoldier with the Developer Config. While in the main menu, press gravekey (~). 
> 2. Type in **g_developer 1** to enable developer mode. **g_developer 0** to disable
> 3. Next you will need to direct ownership of "developer mode" to the player's "Account". Type in ** *"setdbgent Player"* **
> 4. If you would like to see debug info, type in ** *"startdebug"* **.
> 5. You should now be able to use all known dev & debug commands. For example, type in ** *"Map Train"* **. This will jump you to the start of "Train" level. You can jump to any map as long as you know the name of it.

### Known Map Names

**Singleplayer Map Names:**

> - Anenerbe
> - Anenerbe2
> - Berlin
> - hq_1
> - hq_2
> - LHasa01
> - LHasa02
> - LHasa03
> - minarett
> - minarett2
> - nazirat
> - racing
> - t9_1
> - t9_2
> - zastavka

**Multiplayer Map Names:**

> - mp_1
> - mp_berlin_01
> - mp_hq_1
> - mp_hq_2
> - mp_LHasa_02
> - mp_LHasa_03
> - mp_minarett
> - mp_minarett2
> - mp_nazirat


### Info:

UberSoldier 2 was developed by Burut Creative and published by Strategy First | CDV Software

Steam Store:
http://store.steampowered.com/app/281410/